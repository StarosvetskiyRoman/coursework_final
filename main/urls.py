from django.conf.urls import url, include
from django.contrib.auth.views import logout

from main import views
from userpage.views import get_workbook_view, edit_workbook
from main.models import Workbook

urlpatterns = [
    url(r'^$', views.main, name='main'),
    url(r'^logout', logout, {'next_page': "/main"}, name='logout'),
    url(r'^(?P<pk>\d+)/$', get_workbook_view),
    url(r'^(?P<pk>\d+)/edit', edit_workbook),
    url(r'^tagged/(?P<pk>\d+)', views.tagged_view),
    url(r'^api/like/(?P<pk>\d+)/$', views.WorkbookLikeAPI.as_view(), name='like-api-toogle'),
    url(r'^generate/word/(?P<pk>\d+)/$', views.generate_word, name='generator_word_url'),
    url(r'^workbook/content/(?P<pk>\d+)/$', views.print_workbook_content, name='workbook_content'),
    url(r'^workbook/content/download/workbook/(?P<pk>\d+)/$', views.download_file, name='download_pdf'),
]