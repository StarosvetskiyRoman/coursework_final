# Generated by Django 2.0.3 on 2018-03-28 21:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_workbook_tag_field'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='like',
            name='user',
        ),
        migrations.RemoveField(
            model_name='like',
            name='workbook',
        ),
        migrations.DeleteModel(
            name='Like',
        ),
    ]
