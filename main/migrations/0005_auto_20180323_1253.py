# Generated by Django 2.0.3 on 2018-03-23 12:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_workbook_comments'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workbook',
            name='comments',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Comments'),
        ),
    ]
