# Generated by Django 2.0.3 on 2018-03-28 14:07

from django.db import migrations
import tagging.fields


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0021_auto_20180327_2203'),
    ]

    operations = [
        migrations.AddField(
            model_name='workbook',
            name='tag_field',
            field=tagging.fields.TagField(blank=True, max_length=255),
        ),
    ]
