from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.http import HttpResponseRedirect
from django.utils import timezone
from star_ratings.models import Rating

from Comments.models import Comment
from tagging.fields import TagField
from tagging.registry import register
from django.shortcuts import reverse
from django.contrib.contenttypes.models import ContentType


class Workbook(models.Model):

    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    description = models.CharField(max_length=5000)
    speciality = models.IntegerField()
    text = models.TextField()
    rating = GenericRelation(Rating)
    tag_field = TagField()
    likes = models.ManyToManyField('auth.User', related_name='workbook_likes')
    updating_time = models.DateTimeField()

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        reverse('userpage:get_workbook_view', kwargs={"pk": self.pk})

    def get_absolute_api_url(self):
        HttpResponseRedirect(reverse('userpage:get_workbook_api_view', kwargs={"pk": self.pk}))

    def get_api_like_url(self):
        return reverse('Comments:like-api-toogle', kwargs={"pk": self.pk})

    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(self)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type


register(Workbook)
