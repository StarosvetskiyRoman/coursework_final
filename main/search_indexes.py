from haystack import indexes

from main.models import Workbook


class WorkbookIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True, template_name='search/workbook_text.txt')
    description = indexes.EdgeNgramField(model_attr='description')
    speciality = indexes.EdgeNgramField(model_attr='speciality')
    tag_field = indexes.EdgeNgramField(model_attr='tag_field')

    def get_model(self):
        return Workbook

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

