import mimetypes
from wsgiref.util import FileWrapper

import os
import pdfkit as pdfkit
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from tagging.managers import ModelTaggedItemManager
from tagging.models import Tag

from main.models import Workbook

NUMBER_OF_TAGS_IN_CLOUD = 100
DEFAULT_PAGE_NUMBER_OF_WORKBOOKS = 20


def get_cloud_tags(count=NUMBER_OF_TAGS_IN_CLOUD):
    cloud = Workbook.tags.cloud()
    if len(cloud) <= count:
        return cloud
    else:
        return cloud[:count]


def main(request):
    user = request.user
    tag_cloud = get_cloud_tags()
    workbooks = Workbook.objects.order_by('description')[:DEFAULT_PAGE_NUMBER_OF_WORKBOOKS]
    if request.POST:
        if 'theme' in request.POST:
            try:
                del request.session['theme']
            except KeyError:
                request.session['theme'] = 'any'
    return render(request, 'main/main.html', {'workbooks': workbooks, 'user': user, 'cloud': tag_cloud})


def tagged_view(request, pk):
    user = request.user
    tag_cloud = get_cloud_tags()
    manager = ModelTaggedItemManager()
    workbooks = manager.with_any(Tag.objects.get(pk=pk), queryset=Workbook.objects.all())
    return render(request, 'main/main.html',
                  {'workbooks': workbooks, 'user': user, 'cloud': tag_cloud, 'tag': Tag.objects.get(pk=pk)})


class WorkbookLikeAPI(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk):
        obj = get_object_or_404(Workbook, pk=pk)
        user = self.request.user
        updated = False
        liked = False
        if user.is_authenticated:
            if user in obj.likes.all():
                liked = False
                obj.likes.remove(user)
            else:
                liked = True
                obj.likes.add(user)
            updated = True
        data = {
            "updated": updated,
            "liked": liked,
        }
        return Response(data)


def generate_word(request, pk):
    path_wkthmltopdf = b'C:\\Program Files (x86)\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=path_wkthmltopdf)
    pdfkit.from_url('127.0.0.1:8000' + reverse('workbook_content', kwargs={"pk": pk}),
                    'workbook.pdf', )


def print_workbook_content(request, pk):
    workbook = Workbook.objects.get(pk=pk)

    return render(request, "workbook/workbook_content.html", {'workbook': workbook})


def download_file(request, pk):
    path_wkthmltopdf = b'C:\\Program Files (x86)\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=path_wkthmltopdf)
    pdfkit.from_url('127.0.0.1:8000' + reverse('workbook_content', kwargs={"pk": pk}),
                    'workbook.pdf', configuration=config)

    workbook = Workbook.objects.get(pk=pk)
    filename = r"c:\\Users\\Chrv\\CourseWork1\\workbook.pdf"
    download_name = "workbook " + str(workbook.pk) + " " + str(workbook.author.username) + ".pdf"
    wrapper = FileWrapper(open(filename, mode='rt', encoding="iso8859_5"))
    content_type = mimetypes.guess_type(filename)[0]
    response = HttpResponse(wrapper, content_type=content_type)
    response['Content-Length'] = os.path.getsize(filename)
    response['Content-Disposition'] = "attachment; filename=%s" % download_name
    return response