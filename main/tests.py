from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone
from tagging.managers import ModelTaggedItemManager

from main.models import Workbook
from main.views import get_cloud_tags


class TestView(TestCase):

    def setUp(self):
        user1 = User(username='user1')
        user2 = User(username='user2')
        user1.save()
        user2.save()
        Workbook.objects.create(author=user1, description="First workbook", speciality=123,
                                text="First workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())
        Workbook.objects.create(author=user1, description="Second workbook", speciality=456,
                                text="Second workbook text",
                                tag_field="six seven house", updating_time=timezone.now())
        Workbook.objects.create(author=user2, description="Third workbook", speciality=789,
                                text="Third workbook text",
                                tag_field="itra nothing computer", updating_time=timezone.now())
        Workbook.objects.create(author=user2, description="Fourth workbook", speciality=321,
                                text="Fourth workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())
        Workbook.objects.create(author=user1, description="Fifth workbook", speciality=579,
                                text="Fifth workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())

    def test_main(self):
        response = self.client.get('/main/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateNotUsed(response, 'main/main.html')
        self.assertTemplateNotUsed(response, 'main_base.html')
        self.assertRedirects(response, '/ru/main/')
        response = self.client.get('/ru/main/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/main.html')
        self.assertTemplateUsed(response, 'main_base.html')
        self.assertEqual(Workbook.objects.all().count(), 5)
        self.assertQuerysetEqual(response.context['workbooks'],
                                 [repr(workbook) for workbook in Workbook.objects.all()], ordered=False)
        self.assertQuerysetEqual(response.context['cloud'], [repr(tag) for tag in get_cloud_tags()])

    def test_get_cloud_tags(self):
        self.assertEqual(get_cloud_tags(), Workbook.tags.cloud())

    def test_tagged_view(self):
        cloud = get_cloud_tags()
        for tag in cloud:
            manager = ModelTaggedItemManager()
            response = self.client.get('/ru/main/tagged/'+str(tag.pk))
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'main/main.html')
            self.assertTemplateUsed(response, 'main_base.html')
            self.assertQuerysetEqual(response.context['workbooks'],
                                     [repr(workbook) for workbook in manager.with_any(tag, queryset=Workbook.objects.all())], ordered=False)
