
$(document).ready(function($){
    $(".comment-reply-btn").click(function (event) {
        event.preventDefault();
        $('html').scrollTop(99999);
        var id = $(this).attr("data-href");
        $("#greenLion").val(id);
    });

    $('a.like-btn').on('click',(function (e) {
        e.preventDefault();
        var this_ = $(this);
        var likeUrl = this_.attr("data-href");
        var likeCount = parseInt(this_.attr("data-likes")) | 0;
        var addLike = likeCount + 1;
        var deleteLike = likeCount - 1;
        $.ajax({
            url: likeUrl,
            method: "GET",
            data: {},
            success: function (data) {
                console.log(data);
                var newLikes = 0;
                if (data.liked) {
                    newLikes = likeCount + 1;
                    updateText(this_, "Likes: ", addLike)
                } else {
                    newLikes = likeCount - 1;
                    updateText(this_, "Likes: ", deleteLike)
                }
            }, error: function (error) {
                console.log(error);
                console.log("error")
            }
        })
    })
    );
});