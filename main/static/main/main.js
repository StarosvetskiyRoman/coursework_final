//function create_events() {
$(document).ready(function ($) {

    function updateText(btn, newCount, verb) {
        btn.text(newCount + " " + verb);
        btn.attr("data-likes", newCount)
    }

    $('#submit-comment').click(function () {
        $.ajax({
            type: "POST",
            data: $("#comment-form").serialize(),
            url: $(this).attr('data-href'),
            success: function () {
                //console.log('ok');
                $('#comment-form')[0].reset();
            }
        });
        return false;
    });
    $('#submit-reply').click(function () {
        $.ajax({
            type: "POST",
            data: $("#submit-reply-form").serialize(),
            url: $(this).attr('data-href'),
            success: function () {
                //console.log('ok');
                $('#submit-reply-form')[0].reset();
            }
        });
        return false;
    });

    $('#download-word').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/main/workbook/content/download/workbook/' + $('#download-word').attr('data-href'),
            success: function (response) {
                alert(response);
            }
        })
    });
//}

    var pk = $("#submit-reply-form").attr('data-id');

    function update_new_comments() {
        $.ajax({
            url: ("/comment/api/get/" + pk.toString()),
            success: function (data) {
                $('#all_comments').html(data);
            }
        });
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    update_new_comments();
    setInterval(update_new_comments, 2000);
});