from django.contrib import admin
from .models import Workbook
from django_summernote.admin import SummernoteModelAdmin

admin.site.register(Workbook, SummernoteModelAdmin)
