from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone

from main.models import Workbook
from question.forms import QuestionForm, AnswerForm
from question.models import Question


class TestQuestion(TestCase):

    def setUp(self):
        user1 = User.objects.create(username='user1')
        user2 = User.objects.create(username='user2')
        user3 = User.objects.create(username='user3')
        user1.set_password('123')
        user2.set_password('123')
        user3.set_password('123')
        user1.save()
        user2.save()
        user3.save()
        Question.objects.create(author=user1, user_to=user2,
                                question_time=timezone.now(), answer_time=timezone.now())
        Question.objects.create(author=user1, user_to=user3,
                                question_time=timezone.now(), answer_time=timezone.now())
        Question.objects.create(author=user2, user_to=user3,
                                question_time=timezone.now(), answer_time=timezone.now())
        Question.objects.create(author=user2, user_to=user1,
                                question_time=timezone.now(), answer_time=timezone.now())
        Question.objects.create(author=user3, user_to=user1,
                                question_time=timezone.now(), answer_time=timezone.now())
        Question.objects.create(author=user3, user_to=user2,
                                question_time=timezone.now(), answer_time=timezone.now())
        Workbook.objects.create(author=user1, description="First workbook", speciality=123,
                                text="First workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())
        Workbook.objects.create(author=user1, description="Second workbook", speciality=456,
                                text="Second workbook text",
                                tag_field="six seven house", updating_time=timezone.now())
        Workbook.objects.create(author=user2, description="Third workbook", speciality=789,
                                text="Third workbook text",
                                tag_field="itra nothing computer", updating_time=timezone.now())
        Workbook.objects.create(author=user2, description="Fourth workbook", speciality=321,
                                text="Fourth workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())
        Workbook.objects.create(author=user1, description="Fifth workbook", speciality=579,
                                text="Fifth workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())

    def test_asked(self):
        for user in User.objects.all():
            self.client.login(username=user.username, password='123')
            response = self.client.get('/ru/question/asked/')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'question/view_questions.html')
            self.assertQuerysetEqual(response.context['questions'],
                                     [repr(question) for question in Question.objects.filter(user_to=user)],
                                     ordered=False)

    def test_you_ask(self):
        for user in User.objects.all():
            self.client.login(username=user.username, password='123')
            response = self.client.get('/ru/question/you_ask/')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'question/view_questions.html')
            self.assertQuerysetEqual(response.context['questions'],
                                     [repr(question) for question in Question.objects.filter(author=user)],
                                     ordered=False)

    def test_answer_question(self):
        for question in Question.objects.all():
            pk = question.pk
            response = self.client.get('/ru/question/answer/' + str(pk) + '/')
            self.assertEqual(response.context['form'].is_valid(), AnswerForm().is_valid())
            self.assertQuerysetEqual(response.context['questions'],
                                     [repr(question) for question in Question.objects.filter(pk=pk)], ordered=False)

    def test_answer_question_post(self):
        self.assertEqual(Question.objects.all().count(), 6)
        self.assertEqual(Question.objects.filter(answer='qwerty').count(), 0)
        for question in Question.objects.all():
            pk = question.pk
            self.client.post('/ru/question/answer/' + str(pk) + '/', data={'answer': 'qwerty'})
        self.assertEqual(Question.objects.all().count(), 6)
        self.assertEqual(Question.objects.filter(answer='qwerty').count(), 6)

    def test_ask_question(self):
        for workbook in Workbook.objects.all():
            pk = workbook.pk
            response = self.client.get('/ru/question/ask/' + str(pk) + '/')
            self.assertEqual(response.context['form'].is_valid(), QuestionForm().is_valid())
            self.assertEqual(response.context['user'], workbook.author)

    def test_ask_question_post(self):
        self.client.login(username='user1', password='123')
        self.assertEqual(Question.objects.all().count(), 6)
        self.assertEqual(Question.objects.filter(question='qwerty').count(), 0)
        for workbook in Workbook.objects.all():
            pk = workbook.pk
            response = self.client.post('/ru/question/ask/' + str(pk) + '/', data={'question': 'qwerty'})
            self.assertEqual(response.status_code, 302)
        self.assertEqual(Question.objects.all().count(), 11)
        self.assertEqual(Question.objects.filter(question='qwerty').count(), 5)