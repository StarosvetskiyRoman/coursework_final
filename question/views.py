from django.shortcuts import render, redirect
from django.utils import timezone

from question.forms import QuestionForm, AnswerForm

from main.models import Workbook
from question.models import Question


def ask_question(request, pk):
    form = QuestionForm()
    workbook = Workbook.objects.get(pk=pk)
    if request.POST:
        form = QuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.user_to = workbook.author
            question.author = request.user
            question.question_time = timezone.now()
            question.save()
            return redirect('/main/')
    return render(request, 'question/ask_question.html', {'form': form, 'user': workbook.author})


def answer_question(request, pk):
    form = AnswerForm()
    questions = Question.objects.filter(pk=pk)
    if request.POST:
        form = AnswerForm(request.POST)
        if form.is_valid():
            questions.update(answer=request.POST.get('answer', ''), answer_time=timezone.now())
    return render(request, 'question/answer_question.html', {'form': form, 'questions': questions})


def asked(request):
    questions = Question.objects.filter(user_to=request.user)
    return render(request, 'question/view_questions.html', {'questions': questions})


def you_ask(request):
    questions = Question.objects.filter(author=request.user)
    return render(request, 'question/view_questions.html', {'questions': questions})