from question import views
from django.conf.urls import url

urlpatterns = [
    url(r'^ask/(?P<pk>\d+)/$', views.ask_question),
    url(r'^answer/(?P<pk>\d+)/$', views.answer_question),
    url(r'^asked/$', views.asked),
    url(r'^you_ask/$', views.you_ask),
]