from django import forms

from question.models import Question


class QuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ('question',)


class AnswerForm(forms.Form):

    class Meta:
        model = Question
        fields = ('answer',)
