from django.db import models


class Question(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='author')
    user_to = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='user_to', default=None)
    question = models.TextField()
    answer = models.TextField(null=True)
    question_time = models.DateTimeField()
    answer_time = models.DateTimeField(null=True)