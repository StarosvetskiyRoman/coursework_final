"""CourseWork URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from django.conf.urls.static import static
from django.views.generic import RedirectView

from CourseWork import settings

from django.conf.urls import url

urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('main/', include('main.urls'), name='main'),
    path(r'', RedirectView.as_view(url='main/')),
    path('homepage/', include('userpage.urls')),
    path(r'summernote/', include('django_summernote.urls')),
    path(r'auth/', include('auth_it.urls')),
    path(r'ratings/', include('star_ratings.urls', namespace='ratings')),
    url(r'search/?', include('haystack.urls')),
    path(r'comment/', include('Comments.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    path(r'question/', include('question.urls'))
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
