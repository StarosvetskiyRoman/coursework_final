"""
Django settings for CourseWork project.

Generated by 'django-admin startproject' using Django 2.0.3.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from datetime import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'v*u(n)t-mm#z=t8umyufx=^*4i0#9&l8x_7oh=b+g4f(kw^i67'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django_summernote',
    'social_django',
    'star_ratings',
    'haystack',
    'tagging',
    'requests',
    'auth_it',
    'main',
    'Comments',
    'crispy_forms',
    'userpage',
    'question',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
]

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

ROOT_URLCONF = 'CourseWork.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'CourseWork.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'


# folder for images(summernote application)
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')


INTERNAL_IPS = ('127.0.0.1',)

AUTHENTICATION_BACKENDS = (
    'social_core.backends.twitter.TwitterOAuth',
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.vk.VKOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = "/auth/sign_in"
LOGIN_REDIRECT_URL = "/main"
LOGOUT_REDIRECT_URL = "/auth/sign_in"

SOCIAL_AUTH_TWITTER_KEY = 'N7g1pC4hkOSSKbk04DouNW7Uj'
SOCIAL_AUTH_TWITTER_SECRET = 'Vf0jYY1oWT4nxvI5huePEsLANtw7lZPvW5O9RYiM87ck7kncg9'

SOCIAL_AUTH_FACEBOOK_KEY = '578187595869685'  # App ID
SOCIAL_AUTH_FACEBOOK_SECRET = '3f7875e59d90819f16ef59d780612b53'  # App Secret

SOCIAL_AUTH_VK_OAUTH2_API_VERSION = 5.52
SOCIAL_AUTH_VK_OAUTH2_KEY = '6400363'
SOCIAL_AUTH_VK_OAUTH2_SECRET = 'HTzEtstQHYm0wNE8CpjB'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'foritracp@gmail.com'
EMAIL_HOST_PASSWORD = 'gbhfvblf`1'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

FORCE_LOWERCASE_TAGS = True

STAR_RATINGS_STAR_HEIGHT = 16
STAR_RATINGS_STAR_WIDTH = 16

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'haystack',
    },
}

SITE_ID = 1

# список доступных языков
LANGUAGES = (
    ('ru', 'Russian'),
    ('be', 'Belorussian'),
)

LOCALE_PATHS = (
    'locale',
    os.path.join(BASE_DIR, 'locale'),
)
