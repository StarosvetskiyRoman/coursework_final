from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone

from Comments.models import Comment
from main.models import Workbook
from question.models import Question
from userpage import forms
from userpage.forms import FilterForm
from userpage.logic import Logic
from django.contrib.contenttypes.models import ContentType
from Comments.forms import CommentForm
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework.response import Response


def home(request):
    user = request.user
    filter_form = FilterForm()
    unanswered = Question.objects.filter(user_to=request.user, answer__isnull=True)
    if request.POST:
        form = FilterForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['field'] == '1':
                workbooks = Workbook.objects.filter(rating__isnull=False, author=user).order_by('-rating__average')
            if form.cleaned_data['field'] == '2':
                workbooks = Workbook.objects.filter(author=user).order_by('updating_time')
            return render(request, 'userpage/home.html',
                          {'user': user, 'workbooks': workbooks, 'form': filter_form, 'questions': unanswered})
    if not user.is_anonymous:
        workbooks = Workbook.objects.filter(author=user)
    else:
        workbooks = None
    return render(request, 'userpage/home.html', {'user': user, 'workbooks': workbooks, 'form': filter_form, 'questions': unanswered})


def create_new_workbook(request):
    if request.POST:
        form = forms.WorkbookForm(request.POST)
        form.description = request.POST.get('description', '')
        form.speciality = request.POST.get('speciality', '')
        form.tag_field = request.POST.get('tags', '')
        if form.is_valid():
            workbook = form.save(commit=False)
            workbook.author = request.user
            workbook.updating_time = timezone.now()
            workbook.save()
            return redirect('/homepage/')
    form = forms.WorkbookForm()
    return render(request, 'workbook/new_workbook.html', {'form': form})


def profile(request):
    info = {'user': request.user,
            'workbook_count': Workbook.objects.filter(author=request.user).count()}
    try:
        return render(request, 'userpage/profile.html', {'info': info, 'social_info': get_social_info(request)})
    except:
        return render(request, 'userpage/profile.html', {'info': info})


def profile_edit(request):
    valid_info = False
    if request.POST:
        user = request.user
        user.username = request.POST.get('username', '')
        user.first_name = request.POST.get('first_name', '')
        user.last_name = request.POST.get('last_name', '')
        user.save()
        valid_info = True
    return render(request, 'userpage/profile_edit.html', {'user': request.user, 'valid_info': valid_info})


def profile_change_password(request):
    success = False
    error = ''
    if request.POST:
        old_password=request.POST.get('old-password', '')
        password1 = request.POST.get('password1', '')
        password2 = request.POST.get('password2', '')
        user = request.user
        if user.check_password(old_password):
            if password1 == password2:
                user.set_password(password1)
                user.save()
                user = authenticate(username=user.username, password=password1)
                login(request, user)
                success = True
        else:
            error = 'Invalid password'
    return render(request, 'userpage/change_password.html', {'success': success, 'error': error})


def get_social_info(request):
    result = {'network': request.user.social_auth.get().provider}
    if result['network'] == 'twitter':
        result['url'] = 'https://twitter.com'
    elif result['network'] == 'vk-oauth2':
        result['network'] = 'ВКонтакте'
        result['url'] = 'https://vk.com'
    elif result['network'] == 'facebook':
        result['url'] = 'https://facebook.com'
    return result


def get_workbook_view(request, pk):
    workbook = Workbook.objects.get(pk=pk)
    user = request.user
    if request.POST:
        if 'delete' in request.POST:
            workbook.delete()
            return redirect("/homepage")
    comments = workbook.comments
    initial_data = {
        "content_type": workbook.get_content_type,
        "object_id": workbook.pk,
    }
    form = CommentForm(request.POST or None, initial=initial_data)
    if form.is_valid():
        c_type = form.cleaned_data.get("content_type")
        content_type = ContentType.objects.get(model=c_type)
        obj_id = form.cleaned_data.get("object_id")
        content_data = form.cleaned_data.get("content")
        parent_obj = None
        try:
            parent_id = int(request.POST.get("parent_id"))
        except:
            parent_id = None

        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                parent_obj = parent_qs.first()

        new_comment, created = Comment.objects.get_or_create(
            user=request.user,
            content_type=content_type,
            object_id=obj_id,
            content=content_data,
            parent=parent_obj,
        )
        return HttpResponseRedirect(reverse('userpage:get_workbook_view', kwargs={
            "pk": obj_id}))

    return render(request, 'workbook/view_workbook.html',
                  {'workbook': workbook, 'user': user,
                   'comments': comments, 'comment_form': form})


def edit_workbook(request, pk):
    workbook = Workbook.objects.get(pk=pk)
    form = forms.WorkbookForm(
        {'description': workbook.description, 'text': workbook.text, 'tag_field': Logic.tags_to_str(workbook.tags),
         'speciality': workbook.speciality})
    if request.POST:
        form = forms.WorkbookForm(request.POST)
        form.description = request.POST.get('description', '')
        form.speciality = request.POST.get('speciality', '')
        form.tag_field = request.POST.get('tags', '')
        if form.is_valid():
            workbooks = Workbook.objects.filter(pk=workbook.pk)
            del workbook.tags
            workbooks.update(description=form.cleaned_data['description'], text=form.cleaned_data['text'],
                             speciality=form.cleaned_data['speciality'], updating_time=timezone.now())
            workbook.tags = form.cleaned_data['tag_field']
        return redirect('/homepage/' + str(workbook.pk))
    return render(request, 'workbook/editing_workbook.html', {'form': form, 'workbook': workbook})


class CommentsUpdateAPI(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk):
        workbook = Workbook.objects.get(pk=pk)
        user = request.user
        comments = workbook.comments
        initial_data = {
            "content_type": workbook.get_content_type,
            "object_id": workbook.pk,
        }
        form = CommentForm(request.POST or None, initial=initial_data)
        if form.is_valid():
            c_type = form.cleaned_data.get("content_type")
            content_type = ContentType.objects.get(model=c_type)
            obj_id = form.cleaned_data.get("object_id")
            content_data = form.cleaned_data.get("content")
            parent_obj = None
            try:
                parent_id = int(request.POST.get("parent_id"))
            except:
                parent_id = None

            if parent_id:
                parent_qs = Comment.objects.filter(id=parent_id)
                if parent_qs.exists() and parent_qs.count() == 1:
                    parent_obj = parent_qs.first()

            new_comment, created = Comment.objects.get_or_create(
                user=request.user,
                content_type=content_type,
                object_id=obj_id,
                content=content_data,
                parent=parent_obj,
            )
        data = {"comment_form": form}
        return Response(data)
