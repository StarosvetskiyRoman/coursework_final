from django.conf.urls import url

from userpage import views

app_name = "userpage"

urlpatterns = [
    url(r'^$', views.home),
    url(r'^(?P<pk>\d+)/$', views.get_workbook_view, name="get_workbook_view"),
    url(r'new', views.create_new_workbook),
    url(r'(?P<pk>\d+)/edit', views.edit_workbook),
    url(r'^profile/$', views.profile),
    url(r'^profile/edit/$', views.profile_edit),
    url(r'^profile/change_password/$', views.profile_change_password)
]