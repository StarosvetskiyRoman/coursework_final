from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone

from main.models import Workbook
from userpage.forms import WorkbookForm
from userpage.logic import Logic


class TestUserpage(TestCase):

    def setUp(self):
        user1 = User.objects.create(username='user1')
        user1.set_password('123')
        user1.save()
        user2 = User.objects.create(username='user2')
        user2.set_password('123')
        user2.save()
        Workbook.objects.create(author=user1, description="First workbook", speciality=123,
                                text="First workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())
        Workbook.objects.create(author=user1, description="Second workbook", speciality=456,
                                text="Second workbook text",
                                tag_field="six seven house", updating_time=timezone.now())
        Workbook.objects.create(author=user1, description="Third workbook", speciality=789,
                                text="Third workbook text",
                                tag_field="itra nothing computer", updating_time=timezone.now())
        Workbook.objects.create(author=user1, description="Fourth workbook", speciality=321,
                                text="Fourth workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())
        Workbook.objects.create(author=user2, description="Fifth workbook", speciality=579,
                                text="Fifth workbook text",
                                tag_field="one two three four five", updating_time=timezone.now())

    def test_home(self):
        self.client.login(username='user1', password='123')
        response = self.client.get('/ru/homepage/')
        self.assertTemplateUsed(response, 'userpage/home.html')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['user'], User.objects.get(pk=1))
        self.assertQuerysetEqual(response.context['workbooks'], [repr(workbook) for workbook in Workbook.objects.filter(
            author=User.objects.get(pk=1))], ordered=False)

    def test_create_new_workbook(self):
        response = self.client.get('/ru/homepage/new/')
        self.assertEqual(response.context['form'].is_valid(), WorkbookForm().is_valid())

    def test_create_new_workbook_post(self):
        self.client.login(username='user1', password='123')
        response = self.client.post('/ru/homepage/new/',
                                    data={'description': 'desc', 'speciality': 123, 'tag_field': '', 'text': 'qwerty'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Workbook.objects.last(),
                         Workbook.objects.get(description='desc', speciality=123, tag_field='', text='qwerty'))

    def test_profile(self):
        self.client.login(username='user1', password='123')
        response = self.client.get('/ru/homepage/profile/')
        self.assertDictEqual(response.context['info'], {'user': User.objects.get(pk=1),
                                                        'workbook_count': Workbook.objects.filter(
                                                            author=User.objects.get(pk=1)).count()})

    def test_profile_edit(self):
        self.client.login(username='user1', password='123')
        response = self.client.get('/ru/homepage/profile/edit/')
        self.assertEqual(response.context['user'], User.objects.get(pk=1))

    def test_profile_edit_post(self):
        self.client.login(username='user1', password='123')
        response = self.client.post('/ru/homepage/profile/edit/',
                                    data={'username': 'user01', 'first_name': '', 'last_name': '', 'email': ''})
        self.assertEqual(response.context['valid_info'], True)
        self.assertEqual(User.objects.get(pk=1), User.objects.get(username='user01'))

    def test_profile_change_password(self):
        response = self.client.get('/ru/homepage/profile/change_password/')
        self.assertEqual(response.context['success'], False)

    def test_profile_change_password_post(self):
        self.client.login(username='user1', password='123')
        start_password_hash = User.objects.get(username='user1').password
        response = self.client.post('/ru/homepage/profile/change_password/',
                                    data={'password1': 'asd', 'password2': 'asd'})
        self.assertNotEqual(start_password_hash, User.objects.get(username='user1').password)
        self.assertEqual(response.context['success'], True)

    def test_edit_workbook(self):
        for workbook in Workbook.objects.all():
            response = self.client.get('/ru/main/' + str(workbook.pk) + '/edit')
            form = WorkbookForm(
                {'description': workbook.description, 'text': workbook.text,
                 'tag_field': Logic.tags_to_str(workbook.tags),
                 'speciality': workbook.speciality})
            self.assertEqual(response.context['workbook'], Workbook.objects.get(pk=workbook.pk))
            self.assertEqual(response.context['form'].is_valid(), form.is_valid())

    def test_edit_workbook_post(self):
        self.assertEqual(Workbook.objects.filter(description='desc').count(), 0)
        for workbook in Workbook.objects.all():
            self.client.post('/ru/main/' + str(workbook.pk) + '/edit',
                             data={'description': 'desc', 'speciality': 123, 'tag_field': '',
                                   'text': 'qwerty'})
        self.assertEqual(Workbook.objects.filter(description='desc').count(), 5)
