from django.utils import timezone

from main.models import Workbook


class Logic:

    @staticmethod
    def tags_to_str(tags):
        res = ''
        for name in tags:
            res += str(name)+' '
        return res
