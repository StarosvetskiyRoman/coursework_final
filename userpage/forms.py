from django import forms
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget

from main.models import Workbook


class WorkbookForm(forms.ModelForm):

    class Meta:
        model = Workbook
        fields = ('description', 'speciality', 'text', 'tag_field')
        widgets = {
            'text': SummernoteWidget(),
        }


CHOICES = ((1, ("рейтингу")), (2, ("дате создания")))


class FilterForm(forms.Form):

    field = forms.ChoiceField(choices=CHOICES)
