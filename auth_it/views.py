import re

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail
from django.db import IntegrityError
from django.http import HttpResponse
from django.shortcuts import render, redirect, reverse
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from .token import account_activation_token


def sign_in(request):
    errors = []
    form = {}

    if request.POST:
        form['name'] = request.POST.get('name')
        form['password'] = request.POST.get('password')

        if form:
            user = authenticate(username=form['name'], password=form['password'])
            if not user:
                errors.append("Логин или пароль введены неверно")
            else:
                login(request, user)
                return redirect(reverse('main'))
        return render(request, 'auth_it/sign_in.html', {'errors': errors, 'form': form})
    return render(request, 'auth_it/sign_in.html', {'errors': errors, 'form': form})


def sign_up(request):
    errors = []
    form = {}
    try:
        if request.POST:
            form['name'] = request.POST.get('name')
            form['email'] = request.POST.get('email')
            form['password'] = request.POST.get('password')
            form['c_password'] = request.POST.get('c_password')
            current_site = get_current_site(request)

            if not form['name']:
                errors.append('Input name')
            if len(form['name']) < 4:
                errors.append('Логин должен содержать больше 4 символов')
            if re.match("^\d+?$", form['name']):
                errors.append('Логин не может содержать только цифры.')
            if re.match("(?=.*[!@#$%^&*])", form['name']):
                errors.append('Логин может содержать только цифры и буквы.')
            if '@' not in form['email']:
                errors.append('Incorrect email')
            if not form['password']:
                errors.append('Input a password')
            if form['password'] != form['c_password']:
                errors.append('Passwords must match')
            if len(form['password']) < 8:
                errors.append('Password must be bigger then 8 symbols')
            if re.match("^\d+?$", form['password']):
                errors.append('Пароль не может содержать только цифры.')
            if re.match("(?=.*[!@#$%^&*])", form['password']):
                errors.append('Пароль может содержать только цифры и буквы.')
            if re.match("^[A-Za-z]+?$", form['password']):
                errors.append('Пароль не может содержать только буквы.')
            if not errors and form:
                user = User.objects.create_user(username=form['name'], password=form['password'], email=form['email'])
                user.is_active = False
                user.save()
                context = {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                    'token': account_activation_token.make_token(user),
                }
                message = render_to_string('auth_it/account_activate_mail.html', context=context)
                send_mail('Activate your iShare account.', message, 'foritracp@gmail.com', [form['email']],
                          fail_silently=False)
                return render(request, 'auth_it/confirm_email_pls.html', {'already_user': request.POST.get('user')})
            else:
                return render(request, 'auth_it/sign_up.html', {'errors': errors, 'form': form})
        else:
            return render(request, 'auth_it/sign_up.html', {'errors': errors, 'form': form})
    except IntegrityError:
        errors.append('User already exists')

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64).decode())
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        # return redirect('home')
        return redirect(reverse('main'))
    else:
        return HttpResponse('Activation link is invalid!')
