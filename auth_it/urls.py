from django.conf.urls import url, include
from django.contrib.auth.views import (
    logout, password_reset, password_reset_done,
    password_reset_confirm, password_reset_complete,
)

from auth_it import views

urlpatterns = [
    url(r'^sign_in/$', views.sign_in, name='sign_in'),
    url(r'^sign_up/$', views.sign_up, name='sign_up'),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^sign_out/$', logout, kwargs={'next_page': '/auth/sign_in'}, name='logout'),
    url(r'^reset_password/$', password_reset, name='reset_password'),
    url(r'^reset_password/done$', password_reset_done, name='password_reset_done'),
    url(r'^reset_password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^reset_password/complete/$', password_reset_complete, name='password_reset_complete'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
]