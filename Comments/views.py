from django.contrib.contenttypes.models import ContentType
from django.core.serializers import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, render_to_response
from django.urls import reverse
from rest_framework import authentication, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from Comments.forms import CommentForm
from Comments.models import Comment
from main.models import Workbook
from userpage.logic import Logic


class CommentLikeAPI(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk):
        # obj = get_object_or_404(Comment, id=pk)
        obj = Comment.objects.get(id=pk)
        user = self.request.user
        updated = False
        liked = False
        if user.is_authenticated:
            if user in obj.likes.all():
                liked = False
                obj.likes.remove(user)
            else:
                liked = True
                obj.likes.add(user)
            updated = True
        data = {
            "updated": updated,
            "liked": liked,
        }
        return Response(data)


class SetCommentAPI(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, pk):
        workbook = Workbook.objects.get(pk=pk)
        user = request.user
        if request.POST:
            if 'delete' in request.POST:
                Logic.delete_workbook(workbook)
                return redirect("/homepage")
        comments = workbook.comments
        initial_data = {
            "content_type": workbook.get_content_type,
            "object_id": workbook.pk,
        }
        form = CommentForm(request.POST or None, initial=initial_data)
        if form.is_valid():
            c_type = form.cleaned_data.get("content_type")
            content_type = ContentType.objects.get(model=c_type)
            obj_id = form.cleaned_data.get("object_id")
            content_data = form.cleaned_data.get("content")
            parent_obj = None
            try:
                parent_id = int(request.POST.get("parent_id"))
            except:
                parent_id = None

            if parent_id:
                parent_qs = Comment.objects.filter(id=parent_id)
                if parent_qs.exists() and parent_qs.count() == 1:
                    parent_obj = parent_qs.first()

            new_comment, created = Comment.objects.get_or_create(
                user=request.user,
                content_type=content_type,
                object_id=obj_id,
                content=content_data,
                parent=parent_obj,
            )


def get_comments(request, pk):
    workbook = Workbook.objects.get(pk=pk)
    user = request.user
    if request.POST:
        if 'delete' in request.POST:
            Logic.delete_workbook(workbook)
            return redirect("/homepage")
    comments = workbook.comments
    initial_data = {
        "content_type": workbook.get_content_type,
        "object_id": workbook.pk,
    }
    form = CommentForm(request.POST or None, initial=initial_data)
    if form.is_valid():
        c_type = form.cleaned_data.get("content_type")
        content_type = ContentType.objects.get(model=c_type)
        obj_id = form.cleaned_data.get("object_id")
        content_data = form.cleaned_data.get("content")
        parent_obj = None
        try:
            parent_id = int(request.POST.get("parent_id"))
        except:
            parent_id = None

        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                parent_obj = parent_qs.first()

        new_comment, created = Comment.objects.get_or_create(
            user=request.user,
            content_type=content_type,
            object_id=obj_id,
            content=content_data,
            parent=parent_obj,
        )

        return HttpResponseRedirect(reverse('get_comments', kwargs={
            "pk": obj_id}))  # HttpResponseRedirect(new_comment.content_object.get_absolute_url())

    return render(request, 'workbook/comments.html',
                  {'workbook': workbook, 'user': user,
                   'comments': comments, 'comment_form': form})
