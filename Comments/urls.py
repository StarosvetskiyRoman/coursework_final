from django.conf.urls import url

from .views import CommentLikeAPI, get_comments, SetCommentAPI

app_name = "Comments"

urlpatterns = [
    url(r'^api/like/(?P<pk>\d+)/$', CommentLikeAPI.as_view(), name='like-api-comment-toogle'),
    url(r'^api/get/(?P<pk>\d+)/$', get_comments, name='get_comments'),
    url(r'^api/set/(?P<pk>\d+)/$', SetCommentAPI.as_view(), name='set_comments')
]