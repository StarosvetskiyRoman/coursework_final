from django.contrib.auth.models import User
from django.test import TestCase
from django.utils import timezone
from main.models import Workbook
from .models import Comment
from django.test.client import RequestFactory, Client
from .forms import CommentForm


class CommentNest(TestCase):

    def setUp(self):
        user1 = User.objects.create(username='user1', password='123')
        user2 = User.objects.create(username='user2', password='123')
        user3 = User.objects.create(username='user3', password='123')

        user1.save()
        user2.save()
        user3.save()

        workbook1 = Workbook(author=user1, description='smth', speciality='123', text='smth',
                             updating_time=timezone.now())
        workbook2 = Workbook(author=user2, description='smth', speciality='123', text='smth',
                             updating_time=timezone.now())
        workbook3 = Workbook(author=user3, description='smth', speciality='123', text='smth',
                             updating_time=timezone.now())

        comment1 = Comment(user=user1, content_type=workbook1.get_content_type, content="smth", object_id=1, )
        comment2 = Comment(user=user2, content_type=workbook1.get_content_type, content="smth", object_id=1, )
        comment3 = Comment(user=user3, content_type=workbook1.get_content_type, content="smth", object_id=1, )
        comment_parent = Comment(user=user3, content_type=workbook1.get_content_type, content="smth", object_id=1,
                                 parent_id=2)
        self.factory = RequestFactory()

    def test_get_comments(self):
        for user in User.objects.all():
            self.client.login(username=user.username, password='123')
            for workbook in Workbook.objects.all():
                response = self.client.get('/ru/comment/api/get/' + workbook.pk)
                self.assertEqual(response.status_code, 200)
                self.assertTemplateUsed(response, 'workbook/comments.html')
                self.assertQuerysetEqual(response.context['comments'], [repr(comment) for comment in workbook.comments],
                                         ordered=False)

    def test_form_comment(self):
        for workbook in Workbook.objects.all():
            content_type = workbook.get_content_type
            object_id = workbook.pk
            content = 'I\'l going fuck yourself '
            form_data = {'content_type': content_type, 'object_id': object_id, 'content': content}
            form = CommentForm(data=form_data)
            self.assertFalse(form.is_valid())

    def test_parent_comment(self):
        for workbook in Workbook.objects.all():
            for comment in workbook.comments:
                for child in comment.children():
                    self.assertFalse(child.is_parent)

    def test_set_coment(self):
        c = Client()
        for workbook in Workbook.objects.all():
            content_type = workbook.get_content_type
            object_id = workbook.pk
            content = 'I\'l going fuck yourself '
            form_data = {'content_type': content_type, 'object_id': object_id, 'content': content}
            form = CommentForm(data=form_data)
            parent_id = 1
            response = c.post('/ru/comment/api/set/' + str(workbook.pk), {'form': form, 'parent_id': parent_id})
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'workbook/comments.html')

    def test_likes(self):
        c = Client()
        for workbook in Workbook.objects.all():
            for comment in workbook.comments:
                response = c.post('/comment/api/like/' + str(comment.pk))
                self.assertEqual(response.status_code, 301)

